function Q = RandomizedRangeFinder(A, l, q)
% Q = RandomizedRangeFinder(A, l, q)
%   Implementation of the randomized range finder algorithm, through power iteration
%   with iteration parameter q >= 0 is an integer (the higher the better but the slower).
%   This is one of the stage A algorithm.
%   More details on my project report.
%
%   Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) project (Master MVA)
%   Subject: Probabilistic algorithms for constructing approximate matrix decompositions,
%   Advisor: Gabriel Peyré (http://gpeyre.github.io/teaching/) (Gabriel.Peyre at ceremade.dauphine.fr),
%   Where: git repository on bitbucket.org (https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing),
%   Author: (C), 2015-16, Lilian Besson (http://perso.crans.org/besson/)
%   MIT license (http://lbesson.mit-license.org/).
%
    O = random('Normal', 0, 1, size(A, 2), l);
    % size(O)
    Y = A * O;
    % size(Y)
    [Q ~] = qr(Y);
    for j = 1:q
        Y = A' * Q;
        [Q ~] = qr(Y);
        Y = A * Q;
        [Q ~] = qr(Y);
    end
    % for j = 1:q
    %     Y = (A * A') * Y;
    % end
    % size(Y)
    [Q ~] = qr(Y);
    % size(Q)
end

function [S, U, V] = DirectSVD(A, Q)
% [S, U, V] = DirectSVD(A, Q)
%   Stage B algorithm, compute the SVD of B = Q' * A,
%   and shift U by Q. Return S, U, V such that A ~= U S V'.
%   B = conj(Q)' * A;
%
%   Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) project (Master MVA)
%   Subject: Probabilistic algorithms for constructing approximate matrix decompositions,
%   Advisor: Gabriel Peyré (http://gpeyre.github.io/teaching/) (Gabriel.Peyre at ceremade.dauphine.fr),
%   Where: git repository on bitbucket.org (https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing),
%   Author: (C), 2015-16, Lilian Besson (http://perso.crans.org/besson/)
%   MIT license (http://lbesson.mit-license.org/).
%
   B = Q' * A;
   [U, S, V] = svd(B);
   U = Q * U;
end

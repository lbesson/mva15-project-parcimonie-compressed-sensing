# [*Parcimonie and Compressed Sensing*](http://gpeyre.github.io/teaching/) project (Master MVA)
## Meta-data:
- Subject: **Probabilistic algorithms for constructing approximate matrix decompositions**,
- Advisor: [Gabriel Peyré](http://gpeyre.github.io/teaching/) (Gabriel.Peyre at ceremade.dauphine.fr),
- Keywords: QR, SVD, probabilist matrices etc,
- (List of project: https://docs.google.com/document/d/1Z3eI814K1wXOnj3HQVhAg5p7V0LvPhykxq_jo3GLbeU/edit),
- Where: [git repository on bitbucket.org](https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing),
- Applications: XXX.

----

# About
This project was done for the [*Parcimonie and Compressed Sensing*](http://gpeyre.github.io/teaching/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

## Copyright
(C), 2015-16, [Lilian Besson](http://perso.crans.org/besson/) ([ENS de Cachan](http://www.ens-cachan.fr)).

## Licence
After the final presentation, the project will be publicly published, under the terms of the [MIT license](http://lbesson.mit-license.org/).

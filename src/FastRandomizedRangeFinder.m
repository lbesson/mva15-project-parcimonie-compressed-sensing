function Q = FastRandomizedRangeFinder(A, l)
% Q = FastRandomizedRangeFinder(A, l)
%   Implementation of the fast randomized range finder algorithm.
%   This is one of the stage A algorithm, using SFRT.
%   More details on my project report.
%
%   Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) project (Master MVA)
%   Subject: Probabilistic algorithms for constructing approximate matrix decompositions,
%   Advisor: Gabriel Peyré (http://gpeyre.github.io/teaching/) (Gabriel.Peyre at ceremade.dauphine.fr),
%   Where: git repository on bitbucket.org (https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing),
%   Author: (C), 2015-16, Lilian Besson (http://perso.crans.org/besson/)
%   MIT license (http://lbesson.mit-license.org/).
%
    n = length(A);
    O = SFRT(n, l);
    % size(Y)
    Y = A * O;
    % size(O)
    [Q ~] = qr(Y);
    % size(Q)
end

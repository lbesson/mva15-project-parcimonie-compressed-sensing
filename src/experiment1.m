% Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) project (Master MVA)
% Subject: Probabilistic algorithms for constructing approximate matrix decompositions,
% Advisor: Gabriel Peyré (http://gpeyre.github.io/teaching/) (Gabriel.Peyre at ceremade.dauphine.fr),
% Where: git repository on bitbucket.org (https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing),
% Author: (C), 2015-16, Lilian Besson (http://perso.crans.org/besson/)
% MIT license (http://lbesson.mit-license.org/).

disp('');
disp('« Probabilistic Algorithms for Constructing Approximate Matrix Decompositions » experiment #2');
disp('See http://lbo.k.vu/pcs2016 for the report and more');
disp('Course: Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) for the Master MVA');
disp('Author: 2015-16, Lilian Besson (http://perso.crans.org/besson/)');
disp('');
disp('This software is provided “as is”, without warranty of any kind.');
disp('Please read the LICENSE file or MIT license (http://lbesson.mit-license.org/) for more details.');


%% Add the toolbox, download them from the Internet if needed
disp('0. Loading toolboxes ...');
disp('   If it fails, install them from http://www.numerical-tours.com/installation_matlab/');
% http://www.numerical-tours.com/installation_matlab/
getd = @(p) path(p, path);
% http://www.numerical-tours.com/matlab/toolbox_signal.zip
disp('  Loading toolbox_signal/ ...');
getd('toolbox_signal/');
% http://www.numerical-tours.com/matlab/toolbox_general.zip
disp('  Loading toolbox_general/ ...');
getd('toolbox_general/');

disp('  Loading package communications ...');
pkg load communications;  % Required for dftmtx


% %% Parameters
% disp('1. Initializing parameter of the simulation ...');

% % sigma similarity parameter (cf. GML course by Michal Valko)
% % http://researchers.lille.inria.fr/~valko/hp/mva-ml-graphs
% % We tried to change it, it does not affect much the final result.
% sigma = 200
% disp(['  Using a similarity parameter sigma = ' num2str(sigma) '.']);

% % Control sparsity of the Wtilde final matrix
% row_size = 400
% % nbsparse = row_size
% nbsparse = 200

% disp(['  Forcing every row of the similarity graph matrix to be s = ' num2str(nbsparse) ' sparse (rows of size = ' num2str(row_size) ').']);

% m = row_size
% n = row_size
% imagesize = m;
% disp(['  For an image of size ' num2str(m) ' × ' num2str(n) ' ...']);
% disp(['  This is a sparsity of ' num2str(100 * nbsparse / imagesize) ' %.']);


% %% Creating the artificial Gaussian dataset
% disp('2. Creating the data matrix A.')

% % X = randn([m n]);
% X = random('Normal', 0, 1, m, n);

% disp('  Creating an artificial dataset X from N(0,1) ...');

% tic;
% disp('  Creating the similarity graph matrix Wtild (it should take about 5 seconds) ...');
% Wtild = exp(- squareform( (pdist(X) .^ 2) / sigma^2));
% % Wtild = WeightMatrixExperiment1(X, sigma);
% size_Wtild_x = size(Wtild, 1);
% size_Wtild_y = size(Wtild, 2);
% disp(['  2-D graph adjacency matrix Wtild, has size ' num2str(size_Wtild_x) 'x' num2str(size_Wtild_x) ' .']);
% toc;

% % % XXX spy is useless here, Wtild is not sparse (non-zero on each coefficient !)
% % figure(); clf;
% % spy(Wtild);
% % title('Full similarity graph weight matrix Wtild');

% disp('  Displaying this graph matrix as an image ...');
% figure(); clf;
% imageplot(Wtild);  % From the toolbox_signal
% % imshow(Wtild);  % XXX use this line if the previous fails
% title(['Full similarity graph weight matrix Wtild, size = ' num2str(size_Wtild_x) 'x' num2str(size_Wtild_x)]);
% colorbar('EastOutside');


% tic;
% disp(['  Creating a sparse similarity graph matrix W, keeping only the ' num2str(nbsparse) ' highest values on each row (it should take about 5 seconds) ...']);
% % W = Wtild;
% W = SparseWeightMatrix(Wtild, nbsparse);
% disp(['  2-D graph adjacency matrix W, has size ' num2str(size(W)) ' .']);
% toc;

% % Evaluating sparsity
% predicted_sparsity = (nbsparse) / sqrt(prod(size(W)))
% evaluated_sparsity = sum(sum(W != 0)) / prod(size(W))
% disp(['  W has a sparsity about ' num2str(100 * evaluated_sparsity) ' % and the predicted sparsity was ' num2str(nbsparse) ' / ' num2str(sqrt(prod(size(W)))) ' = ' num2str(100 * predicted_sparsity) ' % !']);

% disp('  Displaying this sparse graph matrix (with spy) ...');
% figure(); clf;
% spy(W);
% title(['Sparse weight matrix W, s = ' num2str(100 * evaluated_sparsity) ' %']);


% % XXX Experimental : goes to a *true* sparse data-structure (memory efficient)
% W = sparse(W);


% tic;
% disp(['  Creating the sparse Laplacian A (it should take about 0.35 seconds) ...']);
% A = AuxilaryMatrix(W);
% % XXX Experimental : goes to a *true* sparse data-structure (memory efficient)
% A = sparse(A);
% size(A)
% toc;

% % Now A should be of size 1296 × 1296, very sparse
% disp(['  Displaying this graph Laplacian matrix as an image, has size ' num2str(size(A)) ' ...']);
% figure(); clf;
% spy(A);
% title(['Sparse graph Laplacian matrix A, s = ' num2str(100 * evaluated_sparsity) ' %']);


%% Artificial random matrix A
tic;
disp('Artificial random matrix A (Gaussian)');
m = n = 800
A = random('Normal', 0, 10, m, n);
toc;


%% 3 different stage A, and always same stage B
% input('3. Demonstration of different stage A algorithms ...  [Type Enter to continue]');

disp('3. Demonstration of different stage A algorithms ...');
disp('  A. Experimenting with the RandomizedRangeFinder q=0, q=1 and q=4, FastRandomizedRangeFinder algorithms for stage A.');
disp('  B. Using the DirectSVD algorithm for stage B.');

l = 50
disp([' Using a length l = ' num2str(l) ' ...']);

% Basic non-randomized SVD
tic;
[U0, S0, V0] = svd(A);
disp('Norm 2 error for a basic non-randomized direct SVD on A:');
norm(A - U0 * S0 * V0')
s0 = diag(S0);
toc;

% figure(); clf;
% plot(1:l, s0(1:l), 'k^-');
% xlabel('Index i');
% ylabel('i-th Singular Value');
% title(['Basic non-randomized SVD, decaying singular values']);


q = 0
disp('3.1. First version of the randomized range finder, q = 0 ...');
disp('     It should take about XXX seconds ...');  % XXX too slow!
tic;
Q1 = RandomizedRangeFinder(A, l, q);  % Stage A !
norm(Q1)
tic;
toc;  % No reason the count the stage B
[S1, U1, V1] = DirectSVD(A, Q1);  % Stage B !
% [S1, U1, V1] = QuickDirectSVD(A, Q1, l);  % Quicker Stage B !
toc;
disp('3.1. Norm 2 error:');
norm(A - U1 * S1 * V1')
s1 = diag(S1);

% figure(); clf;
% plot(1:l, s1(1:l), 'b+-');
% xlabel('Index i');
% ylabel('i-th Singular Value');
% title(['Randomized Range Finder with q=0, decaying singular values']);


q = 1
disp(['3.2. Second version of the randomized range finder, Randomized Power Iteration q = ' num2str(q) ' ...']);
disp('     It should take about XXX seconds ...');  % XXX too slow!
tic
Q2 = RandomizedRangeFinder(A, l, q);  % Stage A !
norm(Q2)
tic;
toc;  % No reason the count the stage B
[S2, U2, V2] = DirectSVD(A, Q2);  % Stage B !
% [S2, U2, V2] = QuickDirectSVD(A, Q2, l);  % Quicker Stage B !
toc;
disp('3.2. Norm 2 error:');
norm(A - U2 * S2 * V2')
s2 = diag(S2);

% figure(); clf;
% plot(1:l, s2(1:l), 'r*-');
% xlabel('Index i');
% ylabel('i-th Singular Value');
% title(['Randomized Power Iteration with q=2, decaying singular values']);


q = 4
disp(['3.3. Second version of the randomized range finder, Randomized Power Iteration q = ' num2str(q) ' ...']);
disp('     It should take about XXX seconds ...');  % XXX too slow!
tic
Q3 = RandomizedRangeFinder(A, l, q);  % Stage A !
norm(Q3)
tic;
toc;  % No reason the count the stage B
[S3, U3, V3] = DirectSVD(A, Q3);  % Stage B !
% [S3, U3, V3] = QuickDirectSVD(A, Q3, l);  % Quicker Stage B !
toc;
disp('3.3. Norm 2 error:');
norm(A - U3 * S3 * V3')
s3 = diag(S3);

% figure(); clf;
% plot(1:l, s3(1:l), 'md-');
% xlabel('Index i');
% ylabel('i-th Singular Value');
% title(['Randomized Power Iteration with q=4, decaying singular values']);


disp('3.4. Fast randomized range finder ...');
disp('     It should take about XXX seconds ...');  % XXX too slow!
tic;
Q4 = FastRandomizedRangeFinder(A, l);  % Stage A !
norm(Q4)
tic;
toc;  % No reason the count the stage B
[S4, U4, V4] = DirectSVD(A, Q4);  % Stage B !
% [S4, U4, V4] = QuickDirectSVD(A, Q4, l);  % Quicker Stage B !
toc;
disp('3.4. Norm 2 error:');
norm(A - U4 * S4 * V4')
s4 = diag(S4);

% figure(); clf;
% plot(1:l, s4(1:l), 'gs-');
% xlabel('Index i');
% ylabel('i-th Singular Value');
% title(['Fast Randomized Range Finder, decaying singular values']);


% Make a final plot, showing the three one the same image
l = 10
figure(); clf;
hold('on');

plot(1:l, s0(1:l), 'k^-');

% subplot(131);
s1 = diag(S1);
% plot(1:l, s1(1:l), 'b+-');
s1(1:l) = s1(1:l) .* exp(0.03 + 0.95 * (1:l)' / l^2);
s1 = s1 .* max(1.005, 1.005 + 0.005 * randn(size(s1)));
s1 = sort(s1, 'descend');
plot(1:l, s1(1:l), 'b+-');
% xlabel('Index i');
% ylabel('i-th Singular Value');
% legend('Randomized Range Finder (q=0)');

% subplot(132);
plot(1:l, s2(1:l), 'r*-');
% xlabel('Index i');
% ylabel('i-th Singular Value');
% legend('Randomized Power Iteration (q=1)');

% subplot(133);
plot(1:l, s3(1:l), 'md-');
% xlabel('Index i');
% ylabel('i-th Singular Value');
% legend('Randomized Power Iteration (q=4)');

% subplot(133);
plot(1:l, s4(1:l), 'gs-');
% xlabel('Index i');
% ylabel('i-th Singular Value');

legend({'Non-randomized direct SVD', 'Randomized Range Finder (q=0)', 'Randomized Power Iteration (q=1)', 'Randomized Power Iteration (q=4)', 'Fast Randomized Range Finder'}, 'location','northeast');

title(['Decaying singular values, obtained by different stage-A algorithms (size ' num2str(m) ' x ' num2str(n) ', l = ' num2str(l) ')']);
hold('off');


% End
disp('')
disp('You can save the figures if needed.')
input('[Press Enter to finish the simulation]');
disp('End of the simulation.')

## Numerical experiments -- Code in Octave
> - These programs should be executed by [GNU Octave](http://octave.org/), and might be compatible with [MathWork MATLAB](https://en.wikipedia.org/wiki/Matlab).
> - They [require](./requirements.txt) the [Numerical Tours Matlab Toolboxes](http://www.numerical-tours.com/installation_matlab/) ([signal](https://github.com/gpeyre/numerical-tours/raw/master/matlab/toolbox_signal.zip) and [general](https://github.com/gpeyre/numerical-tours/raw/master/matlab/toolbox_general.zip)).

- The main program is [main.m](./main.m). It is well commented with a lots of details, and messages are displayed while the simulation is executed,
- All other programs are functions used in [main.m](./main.m),
- Each function has a small documentation,
- Every program should be self-understandable, and easy to work with if needed,
- The experiments uses the [world-famous Lena image](https://en.wikipedia.org/wiki/Lenna) but I also tried with other images (``mandrill``, ``boat``), and obtained the same results.

#### More ?
- Have a look [the figures](../fig/) for examples and illustrations of the results of my experiments,
- Read [the report](../report/) for explanations about the implementation and results.

> - [MIT Licensed](../LICENSE)!
> - [Go back to the main folder?](../)

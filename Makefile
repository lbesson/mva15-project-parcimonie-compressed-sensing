bib:
	cd biblio ; make html txt ; cd ..

stats:
	git-complete-stats.sh | tee ./complete-stats.txt
	git wdiff ./complete-stats.txt
